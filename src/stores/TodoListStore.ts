import {action, computed, observable} from "mobx";
import {ITask} from "../interfaces/ITask";

class Store{
  @observable IdToTask: Map<number, ITask> =  new Map();
  @observable lastId: number = 0;

  @computed
  get tasks(): ITask[] {
    return Array.from(this.IdToTask.values());
  }

  @action
  addTask = (title: string, description: string) => {
    this.IdToTask.set(this.lastId,
      {
        title,
        description,
        id: this.lastId++,
        date: new Date()
    })
  };

  @action
  removeTask = (id: number) => {
    this.IdToTask.delete(id);
  };

}

export default new Store();