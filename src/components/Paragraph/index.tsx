import * as React from "react";
import styled from "styled-components";

const ParagraphBase = styled.p`
  font-size: 16px;
`;

export default class Paragraph extends React.Component{
  render() {
    return(
      <ParagraphBase>
        {this.props.children}
      </ParagraphBase>
    )
  }
}