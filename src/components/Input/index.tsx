import * as React from "react";
import styled from "styled-components";

const InputBase = styled.input`
  outline: none;
  font-size: 16px;
  padding: 1rem;
  margin-bottom: 10px;
  width: 100%;
`;

interface IProps {
  changeHandler: (value: string) => void;
  placeholder?: string;
  value?: string
}

export default class Input extends React.Component<IProps> {
  render() {
    const {changeHandler} = this.props;
    return(
      <InputBase {...this.props} onChange={(e) => changeHandler(e.target.value)}/>
    )
  }
}