import * as React from "react";
import styled from "styled-components";

const TaskWrapperBase = styled.div`
  width: 300px;
  height: 300px;
  display: flex;
  flex-direction: column;
  overflow-y: scroll;
  margin-bottom: 10px;
  transition: 300ms;
`;

export default class TaskWrapper extends React.Component {
  render() {
    return(
      <TaskWrapperBase>
        {this.props.children}
      </TaskWrapperBase>
    )
  }
}