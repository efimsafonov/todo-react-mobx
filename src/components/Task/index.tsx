import * as React from "react";
import styled from "styled-components";
import Paragraph from "../Paragraph";
import Button from "../Button";

const TaskBase = styled.div`
  padding: 1rem;
  border: 2px solid #444CFF;
  border-radius: 3px;
  margin-bottom: 5px;
  width: 100%;
`;

interface IProps {
  id: number,
  title: string,
  description: string,
  date: Date,
  deleteTask: (id: number) => void
}

export default class Task extends React.Component<IProps>{
  renderTimeDouble = (time: number) => `${`${time}`.length > 1 ? time: `0${time}`}`;

  renderDate = (date: Date) => `${date.getFullYear()}/${date.getMonth()}/${date.getDay()}` +
    ` ${this.renderTimeDouble(date.getHours())}:${this.renderTimeDouble(date.getMinutes())}`;

  render() {
    const {title, date, description, id, deleteTask} =this.props;
    return(
      <TaskBase>
        <Paragraph>title: {title}</Paragraph>
        <Paragraph>description: {description}</Paragraph>
        <Paragraph>Created: {this.renderDate(date)}</Paragraph>
        <Button clickHandler={() => deleteTask(id)}>Delete Task</Button>
      </TaskBase>
    )
  }
}