import * as React from 'react';
import styled, {css} from 'styled-components';

interface IProps{
  inverse?: boolean
  clickHandler?: () => void
  type?: string
}

const ButtonBase = styled.button`
  border: 2px solid #444CFF;
  border-radius: 3px;
  background: transparent;
  padding: 1rem;
  width: 100%;
  color: #444CFF;
  outline: none;
  cursor: pointer;
  text-transform: uppercase;
  transition: 300ms;
  
  &:hover {
    color: #fff;
    background: #444CFF;
  }
  
  ${(props: IProps) => props.inverse && css`
      color: #fff;
      background: #444CFF;
      
      &:hover {
        color: #444CFF;
        background: #fff;
      }
  `}
`;

export default class Button extends React.Component<IProps>{
  render() {
    const {children, clickHandler} = this.props;
    return(
      <ButtonBase {...this.props} onClick={clickHandler}>
        {children}
      </ButtonBase>
      )
  }
}