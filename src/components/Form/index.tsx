import * as React from "react";
import styled from "styled-components";
import {observer} from "mobx-react";
import {action, observable} from "mobx";
import Input from "../Input";
import TextArea from "../TextArea";
import Button from "../Button";

const FormBase = styled.form`
  border: 2px solid #444CFF;
  border-radius: 10px;
  width: 300px;
  padding: 1rem;
`;

interface IProps {
  submitHandler: (title: string, description: string) => void;
}

@observer
export default class Form extends React.Component<IProps> {
  @observable title: string = '';
  @observable description: string = '';

  @action setTitle = (value: string) => this.title = value;
  @action setDescription = (value: string) => this.description = value;
  @action reset = () => {
    this.title = '';
    this.description = '';
  };

  submitForm = (e: React.FormEvent) => {
    e.preventDefault();
    this.props.submitHandler(this.title, this.description);
    this.reset();
  };

  render() {
    return(
      <FormBase onSubmit={this.submitForm}>
        <Input placeholder='Title' value={this.title} changeHandler={this.setTitle}/>
        <TextArea placeholder='Description' value={this.description} changeHandler={this.setDescription}/>
        <Button type='submit'>Create New Task</Button>
      </FormBase>
    )
  }
}