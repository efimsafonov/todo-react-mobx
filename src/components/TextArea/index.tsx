import * as React from "react";
import styled from "styled-components";

const TextAreaBase = styled.textarea`
  font-size: 16px;
  padding: 1rem;
  width: 100%;
  resize: none;
  outline: none;
`;

interface IProps {
  changeHandler: (value: string) => void;
  placeholder?: string;
  value?: string
}

export default class TextArea extends React.Component<IProps>{
  render() {
    const {changeHandler} = this.props;
    return(
      <TextAreaBase {...this.props} onChange={(e) => changeHandler(e.target.value)}/>
    )
  }
}