import * as React from 'react';
import Store from './stores/TodoListStore';
import {observer} from "mobx-react";
import {ITask} from "./interfaces/ITask";
import Task from "./components/Task";
import Paragraph from "./components/Paragraph";
import Form from "./components/Form";
import './style.css';
import TaskWrapper from "./components/TaskWrapper";
import styled from "styled-components";

const AppBase = styled.div`
  width: 100%;
  min-height: 100%; 
  display: flex;
  flex-direction: column;  
  align-items: center;
  justify-content: center; 
`;

@observer
class App extends React.Component {
  renderTasks = (tasks: ITask[]) => tasks ?
    tasks.map(task => <Task key={task.id} deleteTask={Store.removeTask} {...task}/>)
    :
    (<Paragraph>You have no tasks</Paragraph>);

  render() {
    const {tasks, addTask} = Store;
    return (
      <AppBase>
        <TaskWrapper>
          {this.renderTasks(tasks)}
        </TaskWrapper>
        <Form submitHandler={addTask}/>
      </AppBase>
    );
  }
}

export default App;
